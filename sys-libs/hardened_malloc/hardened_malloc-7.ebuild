# Copyright 2019-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A hardened memory allocator for modern systems."
HOMEPAGE="https://github.com/GrapheneOS/hardened_malloc"
SRC_URI="https://github.com/GrapheneOS/hardened_malloc/archive/${PV}.tar.gz"

PATCHES=(
	"${FILESDIR}/0001-Add-ld.so.conf.d-file.patch"
	"${FILESDIR}/0002-set-soname.patch"
)

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
	dolib.so libhardened_malloc.so

	insinto /etc/ld.so.conf.d
	doins 10libhardened_malloc.conf
}

pkg_postinst() {
	ldconfig
}
