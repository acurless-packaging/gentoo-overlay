# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake

DESCRIPTION="Interface to encode and decode audio with AMBE3000 based devices in packet mode."
HOMEPAGE="https://github.com/f4exb/serialDV"
SRC_URI="https://github.com/f4exb/serialDV/archive/v${PV}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""
