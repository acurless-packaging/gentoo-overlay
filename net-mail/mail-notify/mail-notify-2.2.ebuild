# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Send libnotify notifications when mail arrives"
HOMEPAGE="https://git.acurless.dev/acurless/mail-notify"
SRC_URI="https://git.acurless.dev/acurless/mail-notify/-/archive/${PV}/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="x11-libs/libnotify"
RDEPEND="${DEPEND}"
BDEPEND="virtual/pkgconfig"

src_install() {
	dobin mail-notify
}
