# Copyright 2019-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="7"
KERNEL_NAME="zen-hardened"
S=${WORKDIR}/linux-${PV}
ETYPE="headers"
LICENSE="GPL-2"
SLOT="0"
H_SUPPORTEDARCH="amd64"
KEYWORDS="~amd64 -ppc -sparc ~x86"

inherit kernel-2 || die

DESCRIPTION="Full sources for the Gentoo Linux kernel with Win4Lin support"
SRC_URI="https://github.com/awcurless/zen-hardened/archive/v${PV}-zen-hardened.tar.gz -> ${KERNEL_NAME}-${PV}.tar.gz"
PROVIDE="virtual/os-headers sys-kernel/linux-headers"
HOMEPAGE="https://www.kernel.org/ https://www.gentoo.org/"

DEPEND=">=sys-devel/binutils-2.11.90.0.31"

src_unpack() {
	unpack ${KERNEL_NAME}-${PV}.tar.gz

	mv zen-hardened-${PV}-zen-hardened linux-${PV}-headers
	cd linux-${PV}-headers
	S=${WORKDIR}/linux-${PV}-headers
}

src_install() {
	kernel-2_src_install
	find "${ED}" '(' -name '.install' -o -name '*.cmd' ')' -delete
	find "${ED}" -depth -type d -delete 2>/dev/null
}
