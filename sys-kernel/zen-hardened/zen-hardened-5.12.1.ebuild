# Copyright 2019-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="7"
S=${WORKDIR}/linux-${PV}
ETYPE="sources"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 -ppc -sparc ~x86"

inherit kernel-2 || die

DESCRIPTION="Full sources for the Gentoo Linux kernel with Win4Lin support"
SRC_URI="https://github.com/awcurless/zen-hardened/archive/v${PV}-zen-hardened.tar.gz -> zen-hardened-${PV}.tar.gz"
PROVIDE="virtual/linux-sources-3-r104"
HOMEPAGE="https://www.kernel.org/ https://www.gentoo.org/"

DEPEND=">=sys-devel/binutils-2.11.90.0.31"

src_unpack() {
	unpack zen-hardened-${PV}.tar.gz

	mv zen-hardened-${PV}-zen-hardened linux-${PV}
	cd linux-${PV}
	S=${WORKDIR}/linux-${PV}
}

pkg_postrm() {
	kernel-2_pkg_postrm
}
